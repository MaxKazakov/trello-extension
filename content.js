
chrome.runtime.onMessage.addListener(
	// отвечаем на запрос данных по задаче		
	function(request, sender, response) {
		
		if (request.type == 'getTaskInfo'){
			getTaskInfo()					
		}	 

		if (request.type == 'setCardLink'){
			console.log(request)
			setCardLink(request.data)					
		}  
	}
);

function injectScript(file, node) {
    var th = document.getElementsByTagName(node)[0];
    var s = document.createElement('script');
    s.setAttribute('type', 'text/javascript');
    s.setAttribute('src', file);
    th.appendChild(s);
}

function getTaskInfo(){
	document.dispatchEvent(new CustomEvent('GetTaskDataEvent'));		
}

function setCardLink(link){
	document.dispatchEvent(new CustomEvent('SetCardLink', {
		detail: link
	}));		
}

$(document).ready(function(){

	document.addEventListener('InjectEvent', function(e) {
		console.log("context.js is sending message to popup")    	
		chrome.runtime.sendMessage({type: "taskData", data: e.detail}, function(response) {
		  	// do nothing
		});
	});

	injectScript(chrome.extension.getURL('inject.js'), 'body')
})

