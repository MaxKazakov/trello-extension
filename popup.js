// Отправка сообщения в таб
var sendTabMessage = function(type, data, success) {
	chrome.tabs.sendMessage(tab.id, {type: type, data: data}, {}, success)
}

// кнопка запроса данных со СБИСа
var to_trello_btn = $('#to_trello')
// текущий таб
var tab
// идентификатор текущего пользователя в трелло
var my_id

function trelloGet(url) {
	return new Promise(function(resolve, reject) {
		Trello.get(url, function(success){
			resolve(success)
		},
		function(error){
			reject(new Error(error))
		})    
	});
}

function trelloPost(url, data) {
	return new Promise(function(resolve, reject) {
		Trello.post(url, data, function(success){
			resolve(success)
		},
		function(error){
			reject(new Error(error))
		})    
	});
}

$(document).ready(function(){

	$('#options').click(function(e){
		chrome.tabs.create({ 'url': 'chrome://extensions/?options=' + chrome.runtime.id })
	});

	checkUrl()
	// проверяем что находимся на странице документа Сбиса	
	.then(response => checkAuth(), error => {		
		console.log(error.message)
		// не выводим на UI
		throw null
	})
	// проверяем авторизацию в Trello
	.then(response => trelloGet('/members/me'))	
	// получаем данные о себе	 
	.then(response => {
		my_id = response.id	
		console.log(my_id)
		return trelloGet('/member/me/boards')
	})
	// получаем доски
	.then(response =>{
		console.log(response)	
		var savedSelectedBoardId = localStorage["selectedBoardId"]	

		var selectedIdx = 0
		response.forEach(function(item, i, arr){
			var option = document.createElement('option')			

			option.value = item.id
			option.text = item.name
			$('#boards').append(option)

			if (item.id == savedSelectedBoardId){				
				selectedIdx = i
				$('#boards').val(item.id)
			}
		})
		if (response.length == 0){
			throw new Error("Массив с досками пустой")
		}
		console.log('savedSelectedBoardId ' + selectedIdx)
		$('#boards').selectedIndex = selectedIdx

		to_trello_btn.show()	
	})
	// обрабатываем ошибки
	.catch(error => {
		if (error && error.message){
			showMessage(error.message)
		}
		console.log("Ошибка при получении данных Trello: " + error.message)
		to_trello_btn.hide()
	})
	
});

to_trello_btn.click(function(){
	sendTabMessage("getTaskInfo")
})

// Отправляем данные в трелло
$('#sendToTrello').click(function(){
	console.log('send to trello')

	// Получаем первый список выбранной доски
	var selectedIdx = $('#boards').selectedIndex 
	if (selectedIdx < 0){
		showMessage("Доска не выбрана")
	}
	var selected_board_id = $("#boards option:selected").val()

	trelloGet('/boards/' +  selected_board_id + '/lists').then(response => {
		var selected_list = response[0]
		if (!selected_list){
			showMessage("Невалидный список!")
		}

		var newCard = {
			name: $('#taskTitle').val(), 
			idMembers: my_id,
			idList: selected_list.id
		}

		// Создаем каротчку
		trelloPost('/cards', newCard)
		.then(response => {			
			showMessage("Карточка отправлена в Trello!")
			localStorage["selectedBoardId"] = selected_board_id

			var card_id = response.id

			// Делаем вложение в карточке
			trelloPost('/cards/' + card_id + '/attachments', { url : tab.url })
			.catch(error => {
				console.log('Не удалось добавить вложение к карточке. Ошибка: ' + error.message)
			})

			// Запрашиваем url созданной карточки
			trelloGet('/cards/' + card_id + '/shortUrl')
			.then(response => {
				console.log(response._value)
				sendTabMessage("setCardLink", response._value)
			}, error => {
				console.log('Не удалось добавить ссылку на карточку в СБИС. Ошибка: ' + error.message)
			})
		})
		.catch(error => {
			showMessage("Ошибка при отправке данных в Trello")
			console.log(error.message)
		})
	})
	.catch(error => {
		showMessage("Ошибка при запросе списков выбранной доски Trello")
	})	
})

function checkAuth(){
	return new Promise(function(resolve, reject){
		Trello.setKey(trello_app_key)
		Trello.authorize(
	    {
	        name: "Trello Helper Extension",
	        expiration: "never",
	        interactive: false,
	        scope: {read: true, write: true},
	        success: function(){
	        	resolve()
	        },
	        error: function(){	        	
	        	reject(new Error("Вы не авторизованы в Trello"))
	        }
	    })
	})
}

function checkUrl(){
	return new Promise(function(resolve, reject){
		var check = function(url){
			var template = /https:\/\/online\.sbis\.ru\/opendoc\.html\?guid=/g
			return template.test(url)
		}

		var query = { active: true, currentWindow: true }
		chrome.tabs.query(query, function(tabs){
			tab = tabs[0]
			if (!tab){
				reject(new Error("Ошибка при получении текущего tab-а"))
			}
			// console.log("checkUrl")
			if (check(tab.url)){
				resolve()	
			}

			reject(new Error("Данная страница не распознана как документ СБИС"))
		})
	})	
}

chrome.runtime.onMessage.addListener(
	// отвечаем на запрос данных по задаче		
	function(request, sender, response) {
		console.log("Ответ от content.js: ")
		console.log(request)
		if (request.type == "taskData"){

			$('#taskTitle').val(request.data.title)
			$('#confirmSending').show()
		}				   
	}
);

function showMessage(text){
	// Скрываем кнопки и поля с инфой
	$('#confirmSending').hide()
	to_trello_btn.hide()
	// Показываем сообщения
	$('#message').show()	
	$('#message').text(text)	
}


