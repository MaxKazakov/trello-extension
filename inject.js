const CHECKER = 100

$(document).ready(function(){

	document.addEventListener('GetTaskDataEvent', function(e) {
		var temp = /"key":"(.+?)"/    
		sbis_doc_id = temp.exec(window.componentOptions)[1] 
		// console.log(sbis_doc_id)

		console.log("Попытка получить данные из SBIS")
		// Отправляем запрос
		requirejs(['js!WS.Data/Source/SbisService'], function(blo){
		            new blo({
		                endpoint: {
		                    contract: 'Документ',
		                    address: '/service/'
		                }
		            }).call(
		            'ПрочитатьДляУчастника',
		            {
		        "ИдО": sbis_doc_id,
		        "ИмяМетода": "СлужЗап.Список",
		        "ДопПоля": {
		            "ИмяОбъекта": "СлужЗап"
				}
		}).addCallback(
			function(data){
				console.log(arguments)
				console.log("Данные из SBIS получены")
				var title = formatDate(data.getRow().get('Дата')) + ". " + data.getRow().get('РазличныеДокументы.Информация')

				document.dispatchEvent(new CustomEvent('InjectEvent', {
				    detail: {title: title}
				}));
			})
		});
	});


	document.addEventListener('SetCardLink', function(e) {
		console.log('SetCardLink')	
		console.log(e)	
	});
})




function formatDate(date) {

  var dd = date.getDate();
  if (dd < 10) dd = '0' + dd;

  var mm = date.getMonth() + 1;
  if (mm < 10) mm = '0' + mm;

  var yy = date.getFullYear() % 100;
  if (yy < 10) yy = '0' + yy;

  return dd + '.' + mm + '.' + yy;
}
