var bg_send = function(type, obj) {
	if(chrome && chrome.runtime) {
		chrome.runtime.sendMessage({type: type, obj: obj});
	}
}

var trello_app_key = 'c453ae50877b77c09cac96c458db202b'

function init() {
	// Check if page load is a redirect back from the auth procedure
    if (HashSearch.keyExists('token')) {
        Trello.authorize(
            {
                name: "SBIS+Trello",
                expiration: "never",
                interactive: false,
                scope: {read: true, write: true},
                success: function () {},
                error: function () {
                    alert("Failed to authorize with Trello.")
                }
            });
    }

    // Message and button containers
    var lout = $("#trello_helper_loggedout");
    var lin = $("#trello_helper_loggedin");

	console.log("123123")
    // Log in button
    $("#trello_helper_login").click(function () {
    	console.log("qwqqq")
        Trello.setKey(trello_app_key);
        Trello.authorize(
            {
                name: "SBIS+Trello",
                type: "redirect",
                expiration: "never",
                interactive: true,
                scope: {read: true, write: true},
                success: function () {
                	console.log("success")	
                    // Can't do nothing, we've left the page
                },
                error: function () {
                    alert("Failed to authorize with Trello.")
                }
            });
    });

    // Log out button
    $("#trello_helper_logout").click(function () {
        Trello.deauthorize();
        location.reload();
    });

    if (!localStorage.trello_token) {
        $(lout).show();
        $(lin).hide();
    } else {
        $(lout).hide();
        $(lin).show();
    }
}

$(document).ready(init);